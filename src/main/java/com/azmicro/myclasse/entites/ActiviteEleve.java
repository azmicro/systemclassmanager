package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "activiteeleve")
public class ActiviteEleve implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idActiviteEleve;
	
	
	private BigDecimal noteActivite;
	
	private Long statutEleveActivite;
	
	@ManyToOne
	@JoinColumn(name = "idEleve")
	private Eleve eleve;
	
	@ManyToOne
	@JoinColumn(name = "idActivite")
	private Activite activite;
	
	

	public ActiviteEleve() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdActiviteEleve() {
		return idActiviteEleve;
	}

	public void setIdActiviteEleve(Long idActiviteEleve) {
		this.idActiviteEleve = idActiviteEleve;
	}

	public BigDecimal getNoteActivite() {
		return noteActivite;
	}

	public void setNoteActivite(BigDecimal noteActivite) {
		this.noteActivite = noteActivite;
	}

	public Long getStatutEleveActivite() {
		return statutEleveActivite;
	}

	public void setStatutEleveActivite(Long statutEleveActivite) {
		this.statutEleveActivite = statutEleveActivite;
	}

	public Eleve getEleve() {
		return eleve;
	}

	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}

	public Activite getActivite() {
		return activite;
	}

	public void setActivite(Activite activite) {
		this.activite = activite;
	}

	

	
	
	

}
