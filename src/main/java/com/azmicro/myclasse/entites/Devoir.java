package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "devoir")
public class Devoir implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idDevoir;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateDevoir;
	
	@ManyToOne
	@JoinColumn(name = "idMatiere")
	private Matiere matiere;
	
	@OneToMany(mappedBy = "devoir")
	private List<DevoirEleve> devoirEleves;
	
	public Devoir() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdDevoir() {
		return idDevoir;
	}

	public void setIdDevoir(Long idDevoir) {
		this.idDevoir = idDevoir;
	}

	public Date getDateDevoir() {
		return dateDevoir;
	}

	public void setDateDevoir(Date dateDevoir) {
		this.dateDevoir = dateDevoir;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public List<DevoirEleve> getDevoirEleves() {
		return devoirEleves;
	}

	public void setDevoirEleves(List<DevoirEleve> devoirEleves) {
		this.devoirEleves = devoirEleves;
	}
	
	

}
