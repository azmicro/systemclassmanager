package com.azmicro.myclasse.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "matiereoption")
public class MatiereOption implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idMatiereOption;
	
	private Long coefficient;
	
	@ManyToOne
	@JoinColumn(name = "idMatiere")
	private Matiere matiere;
	
	@ManyToOne
	@JoinColumn(name = "idNiveau")
	private Niveau niveau;
	
	

	public MatiereOption() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Long getCoefficient() {
		return coefficient;
	}


	public void setCoefficient(Long coefficient) {
		this.coefficient = coefficient;
	}


	public Matiere getMatiere() {
		return matiere;
	}


	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}


	public Niveau getNiveau() {
		return niveau;
	}


	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}


	public Long getIdMatiereOption() {
		return idMatiereOption;
	}

	public void setIdMatiereOption(Long idMatiereOption) {
		this.idMatiereOption = idMatiereOption;
	}

	
	
	

}
