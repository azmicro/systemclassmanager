package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "matiere")
public class Matiere implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idMatiere;
	
	private String libelleMatiereAr;
	
	private String libelleMatiereFr;
	
	private String abreviationMatier;
	
	@OneToMany(mappedBy = "matiere")
	private List<Devoir> devoirs;
	
	@OneToMany(mappedBy = "matiere")
	private List<Activite> activites;
	
	@OneToMany(mappedBy = "matiere")
	private List<MatiereOption> matiereOptions;
	
	

	public Matiere() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdMatiere() {
		return idMatiere;
	}

	public void setIdMatiere(Long idMatiere) {
		this.idMatiere = idMatiere;
	}

	public String getLibelleMatiereAr() {
		return libelleMatiereAr;
	}

	public void setLibelleMatiereAr(String libelleMatiereAr) {
		this.libelleMatiereAr = libelleMatiereAr;
	}

	public String getLibelleMatiereFr() {
		return libelleMatiereFr;
	}

	public void setLibelleMatiereFr(String libelleMatiereFr) {
		this.libelleMatiereFr = libelleMatiereFr;
	}

	public String getAbreviationMatier() {
		return abreviationMatier;
	}

	public void setAbreviationMatier(String abreviationMatier) {
		this.abreviationMatier = abreviationMatier;
	}

	public List<Devoir> getDevoirs() {
		return devoirs;
	}

	public void setDevoirs(List<Devoir> devoirs) {
		this.devoirs = devoirs;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public List<MatiereOption> getMatiereOptions() {
		return matiereOptions;
	}

	public void setMatiereOptions(List<MatiereOption> matiereOptions) {
		this.matiereOptions = matiereOptions;
	}
	
	
	
	

}
