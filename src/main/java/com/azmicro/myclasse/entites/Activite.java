package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "activite")
public class Activite implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idActivite;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateActivite;
	
	@ManyToOne
	@JoinColumn(name = "idMatiere")
	private Matiere matiere;
	
	@OneToMany(mappedBy = "activite")
	private List<ActiviteEleve> ActiviteEleves;
	
	

	public Activite() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	public Date getDateActivite() {
		return dateActivite;
	}



	public void setDateActivite(Date dateActivite) {
		this.dateActivite = dateActivite;
	}



	public Matiere getMatiere() {
		return matiere;
	}



	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}



	public Long getIdActivite() {
		return idActivite;
	}

	public void setIdActivite(Long idActivite) {
		this.idActivite = idActivite;
	}

	public List<ActiviteEleve> getActiviteEleves() {
		return ActiviteEleves;
	}

	public void setActiviteEleves(List<ActiviteEleve> activiteEleves) {
		ActiviteEleves = activiteEleves;
	}
	
	

}
