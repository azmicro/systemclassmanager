package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "classe")
public class Classe implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idClasse;
	
	private String libelleClasse;
	
	@ManyToOne
	@JoinColumn(name = "idNiveau")
	private Niveau niveau;
	
	@ManyToOne
	@JoinColumn(name = "idAnneeScolaire")
	private AnneeScolaire anneescolaire;
	
	
	@OneToMany(mappedBy = "classe")
	private List<Groupe> groupes;
	

	public Classe() {
		super();
	}

	public Long getIdClasse() {
		return idClasse;
	}

	public void setIdClasse(Long idClasse) {
		this.idClasse = idClasse;
	}

	public String getLibelleClasse() {
		return libelleClasse;
	}

	public void setLibelleClasse(String libelleClasse) {
		this.libelleClasse = libelleClasse;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	public List<Groupe> getGroupes() {
		return groupes;
	}

	public void setGroupes(List<Groupe> groupes) {
		this.groupes = groupes;
	}

	public AnneeScolaire getAnneescolaire() {
		return anneescolaire;
	}

	public void setAnneescolaire(AnneeScolaire anneescolaire) {
		this.anneescolaire = anneescolaire;
	}
	
	

}
