package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "niveau")
public class Niveau implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idNiveau;

	private String libelleNiveau;
	
	private String abreviationNiveau;
	
	@OneToMany(mappedBy = "niveau")
	private List<Classe> classes;
	
	@OneToMany(mappedBy = "niveau")
	private List<MatiereOption> matiereOptions;
	
	
	
	public Niveau() {
		super();
	}

	public List<Classe> getClasses() {
		return classes;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}

	public Long getIdNiveau() {
		return idNiveau;
	}

	public void setIdNiveau(Long idNiveau) {
		this.idNiveau = idNiveau;
	}

	public String getLibelleNiveau() {
		return libelleNiveau;
	}

	public void setLibelleNiveau(String libelleNiveau) {
		this.libelleNiveau = libelleNiveau;
	}

	public String getAbreviationNiveau() {
		return abreviationNiveau;
	}

	public void setAbreviationNiveau(String abreviationNiveau) {
		this.abreviationNiveau = abreviationNiveau;
	}

	public List<MatiereOption> getMatiereOptions() {
		return matiereOptions;
	}

	public void setMatiereOptions(List<MatiereOption> matiereOptions) {
		this.matiereOptions = matiereOptions;
	}
	
	

}
