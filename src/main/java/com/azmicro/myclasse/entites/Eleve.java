package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "eleve")
public class Eleve implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idEleve;
	
	private String codeMassar;
	
	private Long numEleveClasse;
	
	private String nomAr;
	
	private String prenomAr;
	
	private String nomFr;
	
	private String prenomFr;
	
	private String genre;
	
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	
	private String adresse;
	
	private String email;
	
	private String photo;
	
	@ManyToOne
	@JoinColumn(name = "idGroupe")
	private Groupe groupe;
	
	@OneToMany(mappedBy = "eleve")
	private List<DevoirEleve> devoirEleves;
	
	@OneToMany(mappedBy = "eleve")
	private List<ActiviteEleve> activiteEleves;
		

	public Eleve() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdEleve() {
		return idEleve;
	}

	public void setIdEleve(Long idEleve) {
		this.idEleve = idEleve;
	}
	
	public String getCodeMassar() {
		return codeMassar;
	}

	public void setCodeMassar(String codeMassar) {
		this.codeMassar = codeMassar;
	}

	public Long getNumEleveClasse() {
		return numEleveClasse;
	}

	public void setNumEleveClasse(Long numEleveClasse) {
		this.numEleveClasse = numEleveClasse;
	}

	public String getNomAr() {
		return nomAr;
	}

	public void setNomAr(String nomAr) {
		this.nomAr = nomAr;
	}

	public String getPrenomAr() {
		return prenomAr;
	}

	public void setPrenomAr(String prenomAr) {
		this.prenomAr = prenomAr;
	}

	public String getNomFr() {
		return nomFr;
	}

	public void setNomFr(String nomFr) {
		this.nomFr = nomFr;
	}

	public String getPrenomFr() {
		return prenomFr;
	}

	public void setPrenomFr(String prenomFr) {
		this.prenomFr = prenomFr;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public List<DevoirEleve> getDevoirEleves() {
		return devoirEleves;
	}

	public void setDevoirEleves(List<DevoirEleve> devoirEleves) {
		this.devoirEleves = devoirEleves;
	}

	public List<ActiviteEleve> getActiviteEleves() {
		return activiteEleves;
	}

	public void setActiviteEleves(List<ActiviteEleve> activiteEleves) {
		this.activiteEleves = activiteEleves;
	}
		
	

}
