package com.azmicro.myclasse.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idUser;
	
	private String nameUserAr;
	
	private String lastnameUserAr;
	
	private String nameUserFr;
	
	private String lastnameUserFr;
	
	private String password;
	
	private String photo;

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getNameUserAr() {
		return nameUserAr;
	}

	public void setNameUserAr(String nameUserAr) {
		this.nameUserAr = nameUserAr;
	}

	public String getLastnameUserAr() {
		return lastnameUserAr;
	}

	public void setLastnameUserAr(String lastnameUserAr) {
		this.lastnameUserAr = lastnameUserAr;
	}

	public String getNameUserFr() {
		return nameUserFr;
	}

	public void setNameUserFr(String nameUserFr) {
		this.nameUserFr = nameUserFr;
	}

	public String getLastnameUserFr() {
		return lastnameUserFr;
	}

	public void setLastnameUserFr(String lastnameUserFr) {
		this.lastnameUserFr = lastnameUserFr;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
