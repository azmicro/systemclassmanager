package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "anneescolaire")
public class AnneeScolaire implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idAnneeScolaire;
	
	@Temporal(TemporalType.DATE)
	private Date dateDebut;
	
	@Temporal(TemporalType.DATE)
	private Date dateFin;
	
	@OneToMany(mappedBy = "anneescolaire")
	private List<Classe> classes;

	public AnneeScolaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdAnneeScolaire() {
		return idAnneeScolaire;
	}

	public void setIdAnneeScolaire(Long idAnneeScolaire) {
		this.idAnneeScolaire = idAnneeScolaire;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public List<Classe> getClasses() {
		return classes;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}
	
	
}
