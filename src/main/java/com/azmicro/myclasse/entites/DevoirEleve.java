package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "devoireleve")
public class DevoirEleve implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idDevoirEleve;
	
	private BigDecimal noteDevoir;
	
	private Long statutEleveDevoir;
	
	@ManyToOne
	@JoinColumn(name = "idEleve")
	private Eleve eleve;
	
	@ManyToOne
	@JoinColumn(name = "idDevoir")
	private Devoir devoir;
	
	

	public DevoirEleve() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdDevoirEleve() {
		return idDevoirEleve;
	}

	public void setIdDevoirEleve(Long idDevoirEleve) {
		this.idDevoirEleve = idDevoirEleve;
	}

	public BigDecimal getNoteDevoir() {
		return noteDevoir;
	}

	public void setNoteDevoir(BigDecimal noteDevoir) {
		this.noteDevoir = noteDevoir;
	}

	public Long getStatutEleveDevoir() {
		return statutEleveDevoir;
	}

	public void setStatutEleveDevoir(Long statutEleveDevoir) {
		this.statutEleveDevoir = statutEleveDevoir;
	}

	public Eleve getEleve() {
		return eleve;
	}

	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}

	public Devoir getDevoir() {
		return devoir;
	}

	public void setDevoir(Devoir devoir) {
		this.devoir = devoir;
	}
	

}
