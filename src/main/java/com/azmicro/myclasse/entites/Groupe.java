package com.azmicro.myclasse.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "groupe")
public class Groupe implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idGroupe;
	
	private String libelleGroupe;
	
	@OneToMany(mappedBy = "groupe")
	private List<Eleve> eleve;
	
	@ManyToOne
	@JoinColumn(name = "idClasse")
	private Classe classe;
	
	

	public Groupe() {
		super();
		
	}

	public Long getIdGroupe() {
		return idGroupe;
	}

	public void setIdGroupe(Long idGroupe) {
		this.idGroupe = idGroupe;
	}

	public String getLibelleGroupe() {
		return libelleGroupe;
	}

	public void setLibelleGroupe(String libelleGroupe) {
		this.libelleGroupe = libelleGroupe;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public List<Eleve> getEleve() {
		return eleve;
	}

	public void setEleve(List<Eleve> eleve) {
		this.eleve = eleve;
	}
	
		

}
